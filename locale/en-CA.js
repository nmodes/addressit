var parser = require('../parsers/en.js');
var extend = require('cog/extend');

module.exports = function(input, opts) {
  return parser(input, extend({
  	state: {
			"AB": /^Alberta|^AB$/i,
			"BC": /^British\sColumbia|^BC$/i,
			"MB": /^Manitoba|^MB$/i,
			"NB": /^New\sBrunswick|^NB$/i,
			"NL": /^Newfoundland\sand\sLabrador|^NL$/i,
			"NS": /^Nova\sScotia|^NS$/i,
			"NT": /^Northwest\sTerritories|^NT$/i,
			"NU": /^Nunavut|^NU$/i,
			"ON": /^Ontario|^ON$/i,
			"PE": /^Prince\sEdward\sIsland|^PE$/i,
			"QC": /^Quebec|^QC$/i,
			"SK": /^Saskatchewan|^SK$/i,
			"YT": /^Yukon|^YT$/i
  	},
  	country: {
        CA: /(^CANADA|^C\.?A$)/i
    },
		// todo: library doesn't support space-separated canadian zip codes
    rePostalCode: /(^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1}\s*\d{1}[A-Z]{1}\d{1}$)/
		}, opts));
};
