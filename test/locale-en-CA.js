var test = require('tape');

function expect(expected) {
  return require('./helpers/expect')(expected, {
    locale: require('../locale/en-CA.js')
  });
}

test('109 Del Monte Dr. Marion Bridge, New Brunswick B1K 2R7', expect({
  number: '109',
  street: 'Del Monte Dr',
  state: 'NB',
  regions: ['Marion Bridge'],
  postalcode: 'B1K 2R7'
}));

test('109 Del Monte Dr. Marion Bridge, NB B1K 2R7', expect({
  number: '109',
  street: 'Del Monte Dr',
  state: 'NB',
  regions: ['Marion Bridge'],
  postalcode: 'B1K 2R7'
}));

test('109 Del Monte Dr. Marion Bridge NB B1K 2R7', expect({
  number: '109',
  street: 'Del Monte Dr',
  state: 'NB',
  regions: ['Marion Bridge'],
  postalcode: 'B1K 2R7'
}));

test('New Brunswick B1K 2R7', expect({
  "state": "NB",
  "regions": [],
  postalcode: 'B1K 2R7'
}));

test('Toronto', expect({
  "regions": ["Toronto"]
}));

test('New Brunswick', expect({
  "state": "NB"
}));

test('New Brunswick, NB', expect({
  "state": "NB",
  "regions": ["New Brunswick"]
}));

test('New Brunswick, New Brunswick', expect({
  "state": "NB",
  "regions": ["New Brunswick"]
}));


test('Marion Bridge, New Brunswick B1K 2R7', expect({
  "state": "NB",
  "regions": ["Marion Bridge"],
  postalcode: 'B1K 2R7'
}));


test('Marion Bridge B1K 2R7', expect({
  "regions": ["Marion Bridge"],
  postalcode: 'B1K 2R7'
}));

// Check behavior with a failing address
test('BOOM', expect({
  "regions": ["BOOM"],
  postalcode: undefined
}));

// 012345 is not a valid US postal code.
// If we don't recognize the postal code, it goes in the region field.
test('Marion Bridge 012345', expect({
  "regions": ["Marion Bridge 012345"],
  postalcode: undefined
}));
