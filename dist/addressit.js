(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.addressit = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/* jshint node: true */
'use strict';

var reNumeric = /^\d+$/;

/**
  ### Address
**/
function Address(text, opts) {
  if (! (this instanceof Address)) {
    return new Address(text);
  }

  this.text = text;
  this.parts = [];
}

module.exports = Address;
var proto = Address.prototype;


/**
  #### Address#_extractStreetParts(startIndex)

  This function is used to extract from the street type match
  index *back to* the street number and possibly unit number fields.

  The function will start with the street type, then also grab the previous
  field regardless of checks.  Fields will continue to be pulled in until
  fields start satisfying numeric checks.  Once positive numeric checks are
  firing, those will be brought in as building / unit numbers and once the
  start of the parts array is reached or we fall back to non-numeric fields
  then the extraction is stopped.
**/
proto._extractStreetParts = function(startIndex, streetPartsLength) {
  var index = startIndex;
  var streetParts = [];
  var numberParts;
  var parts = this.parts;
  var testFn = function() {
    return true;
  };

  while (index >= 0 && testFn()) {
    var alphaPart = isNaN(parseInt(parts[index], 10));

    if (streetParts.length < streetPartsLength || alphaPart) {
      // add the current part to the street parts
      streetParts.unshift(parts.splice(index--, 1));
    }
    else {
      if (! numberParts) {
        numberParts = [];
      }

      // add the current part to the building parts
      numberParts.unshift(parts.splice(index--, 1));

      // update the test function
      testFn = function() {
        var isAlpha = isNaN(parseInt(parts[index], 10));

        // if we have building parts, then we are looking
        // for non-alpha values, otherwise alpha
        return numberParts ? (! isAlpha) : isAlpha;
      };
    }
  } // while

  this.number = this.number || (numberParts ? numberParts.join('/') : '');
  this.street = streetParts.join(' ').replace(/\,/g, '');
};

/**
  #### Address#clean

  The clean function is used to clean up an address string.  It is designed
  to remove any parts of the text that preven effective parsing of the
  address string.
**/
proto.clean = function(cleaners) {
  // ensure we have cleaners
  cleaners = cleaners || [];

  // apply the cleaners
  for (var ii = 0; ii < cleaners.length; ii++) {
    if (typeof cleaners[ii] == 'function') {
      this.text = cleaners[ii].call(null, this.text);
    }
    else if (cleaners[ii] instanceof RegExp) {
      this.text = this.text.replace(cleaners[ii], '');
    }
  }

  return this;
};

/**
  #### Address#extract(fieldName, regexes)

  The extract function is used to extract the specified field from the raw
  parts that have previously been split from the input text.  If successfully
  located then the field will be updated from the parts and that part removed
  from the parts list.
**/
proto.extract = function(fieldName, regexes) {

  // skip fields which have already been parsed
  if (this[fieldName]) { return this; }

  var match;
  var rgxIdx;
  var ii;
  var lookups = [];

  // if the regexes have been passed in as objects, then convert to an array
  if (typeof regexes == 'object' && typeof regexes.splice == 'undefined') {
    var newRegexes = [];

    // iterate through the keys in the regexes
    for (var key in regexes) {
      newRegexes[newRegexes.length] = regexes[key];
      lookups[newRegexes.length - 1] = key;
    }

    // update the regexes to point to the new regexes
    regexes = newRegexes;
  }

  // iterate over the unit regexes and test them against the various parts
  for (ii = this.parts.length-1; ii >= 0; ii--) {
    for (rgxIdx = 0; rgxIdx < regexes.length; rgxIdx++) {

      // skip fields which have already been parsed
      if (this[fieldName]){ continue; }

      // do not consider the first token for an abbreviated 'state' field
      if (ii === 0 && fieldName === 'state'){
        // only where there are more than one token and the first token
        // is less than or equal to three characters in length.
        if ( this.parts.length > 1 && this.parts[ii].length <= 3 ) {
          continue;
        }
      }

      // execute regex against part
      match = regexes[rgxIdx].exec(this.parts[ii]);

      // if we have a match, then process
      if (match) {
        if (match[2]) {
          // if we have a 2nd capture group, then replace the item with
          // the text of that group
          this.parts.splice(ii, 1, match[2]);
        } else {
          // otherwise, just remove the element from parts
          this.parts.splice(ii, 1);
        }

        // set the field
        this[fieldName] = lookups[rgxIdx] || match[1];
      }

      // special case for states
      // @todo: add code comments
      else if (['state', 'postalcode'].includes(fieldName)) {
        var matchMultiplePart = false;
        var spacesInMatch = regexes[rgxIdx].source.split('\\s').length;
        if (spacesInMatch > 1) {
          var multiplePart = [];
          for (var partJoin = ii; partJoin > ii - spacesInMatch && partJoin >= 0; partJoin--) {
            multiplePart.push(this.parts[partJoin]);
          }
          multiplePart.reverse();
          multiplePart = multiplePart.join(' ');
          matchMultiplePart = regexes[rgxIdx].exec(multiplePart);

          if (matchMultiplePart) {
            // if we have a 2nd capture group, then replace the item with
            // the text of that group
            if (matchMultiplePart[2]) {
              this.parts.splice(ii - spacesInMatch + 1, spacesInMatch, matchMultiplePart[2]);
              ii -= spacesInMatch + 1;
            }
            // otherwise, just remove the element
            else {
              this.parts.splice(ii - spacesInMatch + 1, spacesInMatch);
              ii -= spacesInMatch + 1;
            }

            // set the field
            this[fieldName] = lookups[rgxIdx] || matchMultiplePart[1];
          }
        }
      }
    }
  }

  return this;
};

/**
  #### Address#extractStreet

  This function is used to parse the address parts and locate any parts
  that look to be related to a street address.
**/
proto.extractStreet = function(regexes, reSplitStreet, reNoStreet) {
  var reNumericesque = /^(\d*|\d*\w)$/;
  var parts = this.parts;
  var streetPartsLength = 2;

  // ensure we have regexes
  regexes = regexes || [];

  // This function is used to locate the "best" street part in an address
  // string.  It is called once a street regex has matched against a part
  // starting from the last part and working towards the front. In terms of
  // what is considered the best, we are looking for the part closest to the
  // start of the string that is not immediately prefixed by a numericesque
  // part (eg. 123, 42A, etc).
  function locateBestStreetPart(startIndex) {
    var bestIndex = startIndex;

    // if the start index is less than or equal to 0, then return
    for (var ii = startIndex-1; ii >= 0; ii--) {
      // iterate over the street regexes and test them against the various parts
      for (var rgxIdx = 0; rgxIdx < regexes.length; rgxIdx++) {
        // if we have a match, then process
        if (regexes[rgxIdx].test(parts[ii]) && parts[ii-1] && (! reNumericesque.test(parts[ii-1]))) {
          // update the best index and break from the inner loop
          bestIndex = ii;
          break;
        }
      }
    }

    return bestIndex;
  } // locateBestStreetPart

  // iterate over the street regexes and test them against the various parts
  for (var partIdx = parts.length; partIdx--; ) {
    for (var rgxIdx = 0; rgxIdx < regexes.length; rgxIdx++) {
      // if we have a match, then process
      // if the match is on the first part though, reject it as we
      // are probably dealing with a town name or something (e.g. St George)
      if (regexes[rgxIdx].test(parts[partIdx]) && partIdx > 0) {
        var startIndex = locateBestStreetPart(partIdx);

        // if we are dealing with a split street (i.e. foo rd west) and the
        // address parts are appropriately delimited, then grab the next part
        // also
        if (reSplitStreet.test(parts[startIndex + 1])) {
          streetPartsLength = 3;
          startIndex += 1;
        }

        if (reNoStreet.test(parts[startIndex])) {
          streetPartsLength = 1;
        }

        this._extractStreetParts(startIndex, streetPartsLength);
        break;
      }
    }
  }

  return this;
};

/**
  #### Address#finalize

  The finalize function takes any remaining parts that have not been extracted
  as other information, and pushes those fields into a generic `regions` field.
**/
proto.finalize = function() {
  // update the regions, discarding any empty strings.
  this.regions = this.parts.join(' ').split(/\,\s?/).filter(function (region) {
      return region.length;
  });

  // reset the parts
  this.parts = [];

  return this;
};

/**
  #### Address#split

  Split the address into it's component parts, and remove any empty parts
**/
proto.split = function(separator) {
  // split the string
  var newParts = this.text.split(separator || ' ');

  this.parts = [];
  for (var ii = 0; ii < newParts.length; ii++) {
    if (newParts[ii]) {
      this.parts[this.parts.length] = newParts[ii];
    }
  }

  return this;
};

/**
  #### Address#toString

  Convert the address to a string representation
**/
proto.toString = function() {
  var output = '';

  if (this.building) {
    output += this.building + '\n';
  }

  if (this.street) {
    output += this.number ? this.number + ' ' : '';
    output += this.street + '\n';
  }

  output += this.regions.join(', ') + '\n';

  return output;
};

proto.locate_unit = function(){
	let match=this.text.match(/(\d+\s*-\s*\d+)/);
	if(!match) return;
	match=match[0];
	this.text=this.text.replace(match,'');
	match=match.replace(/\s/g,'').split('-');
	this.number=match[0];
	this.unit=match[1];
}

proto.locate_number = function(){
	if(!this.number && !this.street){ //most probably part with numbers only is the home number
		let number=this.parts.find(part=>part.match(/^\d+$/));
		if(number){
			this.number=number;
			this.parts.splice(this.parts.indexOf(number),1);
		}
	}
}

},{}],2:[function(require,module,exports){
/* jshint node: true */
'use strict';

/**
  # addressit

  AddressIt is a freeform street address parser, that is designed to take a
  piece of text and convert that into a structured address that can be
  processed in different systems.

  The focal point of `addressit` is on the street parsing component, rather
  than attempting to appropriately identify various states, counties, towns,
  etc, as these vary from country to country fairly dramatically. These
  details are instead put into a generic regions array that can be further
  parsed based on your application needs.

  ## Example Usage

  The following is a simple example of how address it can be used:

  ```js
  var addressit = require('addressit');

  // parse a made up address, with some slightly tricky parts
  var address = addressit('Shop 8, 431 St Kilda Rd Melbourne');
  ```

  The `address` object would now contain the following information:

  ```
  { text: '8/431 ST KILDA RD MELBOURNE',
    parts: [],
    unit: 8,
    country: undefined,
    number: 431,
    street: 'ST KILDA RD',
    regions: [ 'MELBOURNE' ] }
  ```

  For more examples, see the tests.

  ## Reference

**/

/**
  ### addressit(input, opts?)

  Run the address parser for the given input.  Optional `opts` can be
  supplied if you want to override the default (EN) parser.

**/
module.exports = function(input, opts) {
  // if no locale has been specified, then use the default vanilla en locale
  var parse = (opts || {}).locale || require('./locale/en-US');

  // parse the address
  return parse(input, opts);
};

},{"./locale/en-US":3}],3:[function(require,module,exports){
var parser = require('../parsers/en.js');
var extend = require('cog/extend');

module.exports = function(input, opts) {
  // parse the base address
  return parser(input, extend({ 
  	state: {
	    AL: /(^alabama|^AL$)/i,
	    AK: /(^alaska|^AK$)/i,
	    AS: /(^american\ssamoa|^AS$)/i,
	    AZ: /(^arizona|^AZ$)/i,
	    AR: /(^arkansas|^AR$)/i,
	    CA: /(^california|^CA$)/i,
	    CO: /(^colorado|^CO$)/i,
	    CT: /(^connecticut|^CT$)/i,
	    DE: /(^delaware|^DE$)/i,
	    DC: /(^district\sof\scolumbia|^DC$)/i,
	    FM: /(^federated\sstates\sof\smicronesia|^FM$)/i,
	    FL: /(^florida|^FL$)/i,
	    GA: /(^georgia|^GA$)/i,
	    GU: /(^guam|^GU$)/i,
	    HI: /(^hawaii|^HI$)/i,
	    ID: /(^idaho|^ID$)/i,
	    IL: /(^illinois|^IL$)/i,
	    IN: /(^indiana|^IN$)/i,
	    IA: /(^iowa|^IA$)/i,
	    KS: /(^kansas|^KS$)/i,
	    KY: /(^kentucky|^KY$)/i,
	    LA: /(^louisiana|^LA$)/i,
	    ME: /(^maine|^ME$)/i,
	    MH: /(^marshall\sislands|^MH$)/i,
	    MD: /(^maryland|^MD$)/i,
	    MA: /(^massachusetts|^MA$)/i,
	    MI: /(^michigan|^MI$)/i,
	    MN: /(^minnesota|^MN$)/i,
	    MS: /(^mississippi|^MS$)/i,
	    MO: /(^missouri|^MO$)/i,
	    MT: /(^montana|^MT$)/i,
	    NE: /(^nebraska|^NE$)/i,
	    NV: /(^nevada|^NV$)/i,
	    NH: /(^new\shampshire|^NH$)/i,
	    NJ: /(^new\sjersey|^NJ$)/i,
	    NM: /(^new\smexico|^NM$)/i,
	    NY: /(^new\syork|^NY$)/i,
	    NC: /(^north\scarolina|^NC$)/i,
	    ND: /(^north\sdakota|^ND$)/i,
	    MP: /(^northern\smariana\sislands|^MP$)/i,
	    OH: /(^ohio|^OH$)/i,
	    OK: /(^oklahoma|^OK$)/i,
	    OR: /(^oregon|^OR$)/i,
	    PW: /(^palau|^PW$)/i,
	    PA: /(^pennsylvania|^PA$)/i,
	    PR: /(^puerto\srico|^PR$)/i,
	    RI: /(^rhode\sisland|^RI$)/i,
	    SC: /(^south\scarolina|^SC$)/i,
	    SD: /(^south\sdakota|^SD$)/i,
	    TN: /(^tennessee|^TN$)/i,
	    TX: /(^texas|^TX$)/i,
	    UT: /(^utah|^UT$)/i,
	    VT: /(^vermont|^VT$)/i,
	    VI: /(^virgin\sislands|^VI$)/i,
	    VA: /(^virginia|^VA$)/i,
	    WA: /(^washington|^WA$)/i,
	    WV: /(^west\svirginia|^WV$)/i,
	    WI: /(^wisconsin|^WI$)/i,
	    WY: /(^wyoming|^WY$)/i
  	},
  	country: {
        USA: /(^UNITED\sSTATES|^U\.?S\.?A?$)/i
    },
    rePostalCode: /(^\d{5}$)|(^\d{5}-\d{4}$)/ }, opts));
               // Postal codes of the form 'DDDDD-DDDD' or just 'DDDDD'
               // 10010 is valid and so is 10010-1234
};

},{"../parsers/en.js":6,"cog/extend":4}],4:[function(require,module,exports){
/* jshint node: true */
'use strict';

/**
## cog/extend

```js
var extend = require('cog/extend');
```

### extend(target, *)

Shallow copy object properties from the supplied source objects (*) into
the target object, returning the target object once completed:

```js
extend({ a: 1, b: 2 }, { c: 3 }, { d: 4 }, { b: 5 }));
```

See an example on [requirebin](http://requirebin.com/?gist=6079475).
**/
module.exports = function(target) {
  [].slice.call(arguments, 1).forEach(function(source) {
    if (! source) {
      return;
    }

    for (var prop in source) {
      target[prop] = source[prop];
    }
  });

  return target;
};
},{}],5:[function(require,module,exports){
/* jshint node: true */
'use strict';

module.exports = function(textRegexes) {
  var regexes = [];
  var reStreetCleaner = /^\^?(.*)\,?\$?$/;
  var ii;

  for (ii = textRegexes.length; ii--; ) {
    regexes[ii] = new RegExp(
      textRegexes[ii].replace(reStreetCleaner, '^$1\,?$'),
      'i'
    );
  } // for

  return regexes;
};
},{}],6:[function(require,module,exports){
/* jshint node: true */
'use strict';

var Address = require('../address');
var compiler = require('./compiler');

// initialise the street regexes
// these are the regexes for determining whether or not a string is a street
// it is important to note that they are parsed through the reStreetCleaner
// regex to become more strict
// this list has been sourced from:
// https://www.propertyassist.sa.gov.au/pa/qhelp.phtml?cmd=streettype
//
// __NOTE:__ Some of the street types have been disabled due to collisions
// with common parts of suburb names.  At some point the street parser may be
// improved to deal with these cases, but for now this has been deemed
// suitable.

var streetRegexes = compiler([
  'ALLE?Y',               // ALLEY / ALLY
  'APP(ROACH)?',          // APPROACH / APP
  'ARC(ADE)?',            // ARCADE / ARC
  'AV(E|ENUE)?',          // AVENUE / AV / AVE
  '(BOULEVARD|BLVD)',     // BOULEVARD / BLVD
  'BROW',                 // BROW
  'BYPA(SS)?',            // BYPASS / BYPA
  'C(AUSE)?WAY',          // CAUSEWAY / CWAY
  '(CIRCUIT|CCT)',        // CIRCUIT / CCT
  'CIRC(US)?',            // CIRCUS / CIRC
  'CL(OSE)?',             // CLOSE / CL
  'CO?PSE',               // COPSE / CPSE
  '(CORNER|CNR)',         // CORNER / CNR
  // 'COVE',                 // COVE
  '(C((OUR)|R)?T|CRT)',   // COURT / CT /CRT
  '(CR|CRES|CRESCENT)',          // CRESCENT / CRES
  'DR(IVE)?',             // DRIVE / DR
  // 'END',                  // END
  'ESP(LANADE)?',        // ESPLANADE / ESP
  // 'FLAT',                 // FLAT
  'F(REE)?WAY',           // FREEWAY / FWAY
  '(FRONTAGE|FRNT)',      // FRONTAGE / FRNT
  // '(GARDENS|GDNS)',       // GARDENS / GDNS
  '(GLADE|GLD)',          // GLADE / GLD
  // 'GLEN',                 // GLEN
  'GR(EE)?N',             // GREEN / GRN
  // 'GR(OVE)?',             // GROVE / GR
  // 'H(EIGH)?TS',           // HEIGHTS / HTS
  '(HIGHWAY|HWY)',        // HIGHWAY / HWY
  '(LANE|LN)',            // LANE / LN
  'LINK',                 // LINK
  'LOOP',                 // LOOP
  'MALL',                 // MALL
  'MEWS',                 // MEWS
  '(PACKET|PCKT)',        // PACKET / PCKT
  'P(ARA)?DE',            // PARADE / PDE
  // 'PARK',                 // PARK
  '(PARKWAY|PKWY)',       // PARKWAY / PKWY
  'PL(ACE)?',             // PLACE / PL
  'PROM(ENADE)?',         // PROMENADE / PROM
  'RES(ERVE)?',           // RESERVE / RES
  // 'RI?DGE',               // RIDGE / RDGE
  'RISE',                 // RISE
  'R(OA)?D',              // ROAD / RD
  'ROW',                  // ROW
  'SQ(UARE)?',            // SQUARE / SQ
  'STREET|STR|ST',        // STREET / STR / ST
  'STRI?P',               // STRIP / STRP
  'TARN',                 // TARN
  'T(ERRA)?CE|TER?R',     // TERRACE / TER / TERR / TCE
  '(THOROUGHFARE|TFRE)',  // THOROUGHFARE / TFRE
  'TRACK?',               // TRACK / TRAC
  'TR(AI)?L',             // TRAIL / TRL
  'T(RUNK)?WAY',          // TRUNKWAY / TWAY
  // 'VIEW',                 // VIEW
  'VI?STA',               // VISTA / VSTA
  'WALK',                 // WALK
  'WA?Y',                 // WAY / WY
  'W(ALK)?WAY',           // WALKWAY / WWAY
  'YARD',                 // YARD
  'BROADWAY'
]);

var reSplitStreet = /^(N|NTH|NORTH|E|EST|EAST|S|STH|SOUTH|W|WST|WEST)\,$/i;
var reNoStreet = compiler(['BROADWAY']).pop();

module.exports = function(text, opts) {
  var address = new Address(text, opts);

  // clean the address
  address
    .clean([
        // remove trailing dots from two letter abbreviations
        function(input) {
            return input.replace(/(\w{2})\./g, '$1');
        },

        // convert shop to a unit format
        function(input) {
            return input.replace(/^\s*SHOP\s?(\d*)\,?\s*/i, '$1/');
        }
    ]);
		
	address.locate_unit();
	
	address

    // split the address
    .split(/\s/)

    // extract the unit
    .extract('unit', [

        (/^(?:\#|APT|APARTMENT)\s?(\d+)/),
        (/^(\d+)\/(.*)/)
    ])

    // extract the street
    .extractStreet(streetRegexes, reSplitStreet, reNoStreet);

  address.locate_number();

  if (opts && opts.state) {
    address.extract('state', opts.state );
  }

  if (opts && opts.country) {
    address.extract('country', opts.country );
  }

  if (opts && opts.rePostalCode) {
    address.extract('postalcode', [ opts.rePostalCode ]);
  }

   // take remaining unknown parts and push them
   return address.finalize();
};

},{"../address":1,"./compiler":5}]},{},[2])(2)
});

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL3Vzci9saWIvbm9kZV9tb2R1bGVzL2Jyb3dzZXJpZnkvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsImFkZHJlc3MuanMiLCJpbmRleC5qcyIsImxvY2FsZS9lbi1VUy5qcyIsIm5vZGVfbW9kdWxlcy9jb2cvZXh0ZW5kLmpzIiwicGFyc2Vycy9jb21waWxlci5qcyIsInBhcnNlcnMvZW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM1VkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKXtmdW5jdGlvbiByKGUsbix0KXtmdW5jdGlvbiBvKGksZil7aWYoIW5baV0pe2lmKCFlW2ldKXt2YXIgYz1cImZ1bmN0aW9uXCI9PXR5cGVvZiByZXF1aXJlJiZyZXF1aXJlO2lmKCFmJiZjKXJldHVybiBjKGksITApO2lmKHUpcmV0dXJuIHUoaSwhMCk7dmFyIGE9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitpK1wiJ1wiKTt0aHJvdyBhLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsYX12YXIgcD1uW2ldPXtleHBvcnRzOnt9fTtlW2ldWzBdLmNhbGwocC5leHBvcnRzLGZ1bmN0aW9uKHIpe3ZhciBuPWVbaV1bMV1bcl07cmV0dXJuIG8obnx8cil9LHAscC5leHBvcnRzLHIsZSxuLHQpfXJldHVybiBuW2ldLmV4cG9ydHN9Zm9yKHZhciB1PVwiZnVuY3Rpb25cIj09dHlwZW9mIHJlcXVpcmUmJnJlcXVpcmUsaT0wO2k8dC5sZW5ndGg7aSsrKW8odFtpXSk7cmV0dXJuIG99cmV0dXJuIHJ9KSgpIiwiLyoganNoaW50IG5vZGU6IHRydWUgKi9cbid1c2Ugc3RyaWN0JztcblxudmFyIHJlTnVtZXJpYyA9IC9eXFxkKyQvO1xuXG4vKipcbiAgIyMjIEFkZHJlc3NcbioqL1xuZnVuY3Rpb24gQWRkcmVzcyh0ZXh0LCBvcHRzKSB7XG4gIGlmICghICh0aGlzIGluc3RhbmNlb2YgQWRkcmVzcykpIHtcbiAgICByZXR1cm4gbmV3IEFkZHJlc3ModGV4dCk7XG4gIH1cblxuICB0aGlzLnRleHQgPSB0ZXh0O1xuICB0aGlzLnBhcnRzID0gW107XG59XG5cbm1vZHVsZS5leHBvcnRzID0gQWRkcmVzcztcbnZhciBwcm90byA9IEFkZHJlc3MucHJvdG90eXBlO1xuXG5cbi8qKlxuICAjIyMjIEFkZHJlc3MjX2V4dHJhY3RTdHJlZXRQYXJ0cyhzdGFydEluZGV4KVxuXG4gIFRoaXMgZnVuY3Rpb24gaXMgdXNlZCB0byBleHRyYWN0IGZyb20gdGhlIHN0cmVldCB0eXBlIG1hdGNoXG4gIGluZGV4ICpiYWNrIHRvKiB0aGUgc3RyZWV0IG51bWJlciBhbmQgcG9zc2libHkgdW5pdCBudW1iZXIgZmllbGRzLlxuXG4gIFRoZSBmdW5jdGlvbiB3aWxsIHN0YXJ0IHdpdGggdGhlIHN0cmVldCB0eXBlLCB0aGVuIGFsc28gZ3JhYiB0aGUgcHJldmlvdXNcbiAgZmllbGQgcmVnYXJkbGVzcyBvZiBjaGVja3MuICBGaWVsZHMgd2lsbCBjb250aW51ZSB0byBiZSBwdWxsZWQgaW4gdW50aWxcbiAgZmllbGRzIHN0YXJ0IHNhdGlzZnlpbmcgbnVtZXJpYyBjaGVja3MuICBPbmNlIHBvc2l0aXZlIG51bWVyaWMgY2hlY2tzIGFyZVxuICBmaXJpbmcsIHRob3NlIHdpbGwgYmUgYnJvdWdodCBpbiBhcyBidWlsZGluZyAvIHVuaXQgbnVtYmVycyBhbmQgb25jZSB0aGVcbiAgc3RhcnQgb2YgdGhlIHBhcnRzIGFycmF5IGlzIHJlYWNoZWQgb3Igd2UgZmFsbCBiYWNrIHRvIG5vbi1udW1lcmljIGZpZWxkc1xuICB0aGVuIHRoZSBleHRyYWN0aW9uIGlzIHN0b3BwZWQuXG4qKi9cbnByb3RvLl9leHRyYWN0U3RyZWV0UGFydHMgPSBmdW5jdGlvbihzdGFydEluZGV4LCBzdHJlZXRQYXJ0c0xlbmd0aCkge1xuICB2YXIgaW5kZXggPSBzdGFydEluZGV4O1xuICB2YXIgc3RyZWV0UGFydHMgPSBbXTtcbiAgdmFyIG51bWJlclBhcnRzO1xuICB2YXIgcGFydHMgPSB0aGlzLnBhcnRzO1xuICB2YXIgdGVzdEZuID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH07XG5cbiAgd2hpbGUgKGluZGV4ID49IDAgJiYgdGVzdEZuKCkpIHtcbiAgICB2YXIgYWxwaGFQYXJ0ID0gaXNOYU4ocGFyc2VJbnQocGFydHNbaW5kZXhdLCAxMCkpO1xuXG4gICAgaWYgKHN0cmVldFBhcnRzLmxlbmd0aCA8IHN0cmVldFBhcnRzTGVuZ3RoIHx8IGFscGhhUGFydCkge1xuICAgICAgLy8gYWRkIHRoZSBjdXJyZW50IHBhcnQgdG8gdGhlIHN0cmVldCBwYXJ0c1xuICAgICAgc3RyZWV0UGFydHMudW5zaGlmdChwYXJ0cy5zcGxpY2UoaW5kZXgtLSwgMSkpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGlmICghIG51bWJlclBhcnRzKSB7XG4gICAgICAgIG51bWJlclBhcnRzID0gW107XG4gICAgICB9XG5cbiAgICAgIC8vIGFkZCB0aGUgY3VycmVudCBwYXJ0IHRvIHRoZSBidWlsZGluZyBwYXJ0c1xuICAgICAgbnVtYmVyUGFydHMudW5zaGlmdChwYXJ0cy5zcGxpY2UoaW5kZXgtLSwgMSkpO1xuXG4gICAgICAvLyB1cGRhdGUgdGhlIHRlc3QgZnVuY3Rpb25cbiAgICAgIHRlc3RGbiA9IGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgaXNBbHBoYSA9IGlzTmFOKHBhcnNlSW50KHBhcnRzW2luZGV4XSwgMTApKTtcblxuICAgICAgICAvLyBpZiB3ZSBoYXZlIGJ1aWxkaW5nIHBhcnRzLCB0aGVuIHdlIGFyZSBsb29raW5nXG4gICAgICAgIC8vIGZvciBub24tYWxwaGEgdmFsdWVzLCBvdGhlcndpc2UgYWxwaGFcbiAgICAgICAgcmV0dXJuIG51bWJlclBhcnRzID8gKCEgaXNBbHBoYSkgOiBpc0FscGhhO1xuICAgICAgfTtcbiAgICB9XG4gIH0gLy8gd2hpbGVcblxuICB0aGlzLm51bWJlciA9IHRoaXMubnVtYmVyIHx8IChudW1iZXJQYXJ0cyA/IG51bWJlclBhcnRzLmpvaW4oJy8nKSA6ICcnKTtcbiAgdGhpcy5zdHJlZXQgPSBzdHJlZXRQYXJ0cy5qb2luKCcgJykucmVwbGFjZSgvXFwsL2csICcnKTtcbn07XG5cbi8qKlxuICAjIyMjIEFkZHJlc3MjY2xlYW5cblxuICBUaGUgY2xlYW4gZnVuY3Rpb24gaXMgdXNlZCB0byBjbGVhbiB1cCBhbiBhZGRyZXNzIHN0cmluZy4gIEl0IGlzIGRlc2lnbmVkXG4gIHRvIHJlbW92ZSBhbnkgcGFydHMgb2YgdGhlIHRleHQgdGhhdCBwcmV2ZW4gZWZmZWN0aXZlIHBhcnNpbmcgb2YgdGhlXG4gIGFkZHJlc3Mgc3RyaW5nLlxuKiovXG5wcm90by5jbGVhbiA9IGZ1bmN0aW9uKGNsZWFuZXJzKSB7XG4gIC8vIGVuc3VyZSB3ZSBoYXZlIGNsZWFuZXJzXG4gIGNsZWFuZXJzID0gY2xlYW5lcnMgfHwgW107XG5cbiAgLy8gYXBwbHkgdGhlIGNsZWFuZXJzXG4gIGZvciAodmFyIGlpID0gMDsgaWkgPCBjbGVhbmVycy5sZW5ndGg7IGlpKyspIHtcbiAgICBpZiAodHlwZW9mIGNsZWFuZXJzW2lpXSA9PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aGlzLnRleHQgPSBjbGVhbmVyc1tpaV0uY2FsbChudWxsLCB0aGlzLnRleHQpO1xuICAgIH1cbiAgICBlbHNlIGlmIChjbGVhbmVyc1tpaV0gaW5zdGFuY2VvZiBSZWdFeHApIHtcbiAgICAgIHRoaXMudGV4dCA9IHRoaXMudGV4dC5yZXBsYWNlKGNsZWFuZXJzW2lpXSwgJycpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gICMjIyMgQWRkcmVzcyNleHRyYWN0KGZpZWxkTmFtZSwgcmVnZXhlcylcblxuICBUaGUgZXh0cmFjdCBmdW5jdGlvbiBpcyB1c2VkIHRvIGV4dHJhY3QgdGhlIHNwZWNpZmllZCBmaWVsZCBmcm9tIHRoZSByYXdcbiAgcGFydHMgdGhhdCBoYXZlIHByZXZpb3VzbHkgYmVlbiBzcGxpdCBmcm9tIHRoZSBpbnB1dCB0ZXh0LiAgSWYgc3VjY2Vzc2Z1bGx5XG4gIGxvY2F0ZWQgdGhlbiB0aGUgZmllbGQgd2lsbCBiZSB1cGRhdGVkIGZyb20gdGhlIHBhcnRzIGFuZCB0aGF0IHBhcnQgcmVtb3ZlZFxuICBmcm9tIHRoZSBwYXJ0cyBsaXN0LlxuKiovXG5wcm90by5leHRyYWN0ID0gZnVuY3Rpb24oZmllbGROYW1lLCByZWdleGVzKSB7XG5cbiAgLy8gc2tpcCBmaWVsZHMgd2hpY2ggaGF2ZSBhbHJlYWR5IGJlZW4gcGFyc2VkXG4gIGlmICh0aGlzW2ZpZWxkTmFtZV0pIHsgcmV0dXJuIHRoaXM7IH1cblxuICB2YXIgbWF0Y2g7XG4gIHZhciByZ3hJZHg7XG4gIHZhciBpaTtcbiAgdmFyIGxvb2t1cHMgPSBbXTtcblxuICAvLyBpZiB0aGUgcmVnZXhlcyBoYXZlIGJlZW4gcGFzc2VkIGluIGFzIG9iamVjdHMsIHRoZW4gY29udmVydCB0byBhbiBhcnJheVxuICBpZiAodHlwZW9mIHJlZ2V4ZXMgPT0gJ29iamVjdCcgJiYgdHlwZW9mIHJlZ2V4ZXMuc3BsaWNlID09ICd1bmRlZmluZWQnKSB7XG4gICAgdmFyIG5ld1JlZ2V4ZXMgPSBbXTtcblxuICAgIC8vIGl0ZXJhdGUgdGhyb3VnaCB0aGUga2V5cyBpbiB0aGUgcmVnZXhlc1xuICAgIGZvciAodmFyIGtleSBpbiByZWdleGVzKSB7XG4gICAgICBuZXdSZWdleGVzW25ld1JlZ2V4ZXMubGVuZ3RoXSA9IHJlZ2V4ZXNba2V5XTtcbiAgICAgIGxvb2t1cHNbbmV3UmVnZXhlcy5sZW5ndGggLSAxXSA9IGtleTtcbiAgICB9XG5cbiAgICAvLyB1cGRhdGUgdGhlIHJlZ2V4ZXMgdG8gcG9pbnQgdG8gdGhlIG5ldyByZWdleGVzXG4gICAgcmVnZXhlcyA9IG5ld1JlZ2V4ZXM7XG4gIH1cblxuICAvLyBpdGVyYXRlIG92ZXIgdGhlIHVuaXQgcmVnZXhlcyBhbmQgdGVzdCB0aGVtIGFnYWluc3QgdGhlIHZhcmlvdXMgcGFydHNcbiAgZm9yIChpaSA9IHRoaXMucGFydHMubGVuZ3RoLTE7IGlpID49IDA7IGlpLS0pIHtcbiAgICBmb3IgKHJneElkeCA9IDA7IHJneElkeCA8IHJlZ2V4ZXMubGVuZ3RoOyByZ3hJZHgrKykge1xuXG4gICAgICAvLyBza2lwIGZpZWxkcyB3aGljaCBoYXZlIGFscmVhZHkgYmVlbiBwYXJzZWRcbiAgICAgIGlmICh0aGlzW2ZpZWxkTmFtZV0peyBjb250aW51ZTsgfVxuXG4gICAgICAvLyBkbyBub3QgY29uc2lkZXIgdGhlIGZpcnN0IHRva2VuIGZvciBhbiBhYmJyZXZpYXRlZCAnc3RhdGUnIGZpZWxkXG4gICAgICBpZiAoaWkgPT09IDAgJiYgZmllbGROYW1lID09PSAnc3RhdGUnKXtcbiAgICAgICAgLy8gb25seSB3aGVyZSB0aGVyZSBhcmUgbW9yZSB0aGFuIG9uZSB0b2tlbiBhbmQgdGhlIGZpcnN0IHRva2VuXG4gICAgICAgIC8vIGlzIGxlc3MgdGhhbiBvciBlcXVhbCB0byB0aHJlZSBjaGFyYWN0ZXJzIGluIGxlbmd0aC5cbiAgICAgICAgaWYgKCB0aGlzLnBhcnRzLmxlbmd0aCA+IDEgJiYgdGhpcy5wYXJ0c1tpaV0ubGVuZ3RoIDw9IDMgKSB7XG4gICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLy8gZXhlY3V0ZSByZWdleCBhZ2FpbnN0IHBhcnRcbiAgICAgIG1hdGNoID0gcmVnZXhlc1tyZ3hJZHhdLmV4ZWModGhpcy5wYXJ0c1tpaV0pO1xuXG4gICAgICAvLyBpZiB3ZSBoYXZlIGEgbWF0Y2gsIHRoZW4gcHJvY2Vzc1xuICAgICAgaWYgKG1hdGNoKSB7XG4gICAgICAgIGlmIChtYXRjaFsyXSkge1xuICAgICAgICAgIC8vIGlmIHdlIGhhdmUgYSAybmQgY2FwdHVyZSBncm91cCwgdGhlbiByZXBsYWNlIHRoZSBpdGVtIHdpdGhcbiAgICAgICAgICAvLyB0aGUgdGV4dCBvZiB0aGF0IGdyb3VwXG4gICAgICAgICAgdGhpcy5wYXJ0cy5zcGxpY2UoaWksIDEsIG1hdGNoWzJdKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBvdGhlcndpc2UsIGp1c3QgcmVtb3ZlIHRoZSBlbGVtZW50IGZyb20gcGFydHNcbiAgICAgICAgICB0aGlzLnBhcnRzLnNwbGljZShpaSwgMSk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBzZXQgdGhlIGZpZWxkXG4gICAgICAgIHRoaXNbZmllbGROYW1lXSA9IGxvb2t1cHNbcmd4SWR4XSB8fCBtYXRjaFsxXTtcbiAgICAgIH1cblxuICAgICAgLy8gc3BlY2lhbCBjYXNlIGZvciBzdGF0ZXNcbiAgICAgIC8vIEB0b2RvOiBhZGQgY29kZSBjb21tZW50c1xuICAgICAgZWxzZSBpZiAoWydzdGF0ZScsICdwb3N0YWxjb2RlJ10uaW5jbHVkZXMoZmllbGROYW1lKSkge1xuICAgICAgICB2YXIgbWF0Y2hNdWx0aXBsZVBhcnQgPSBmYWxzZTtcbiAgICAgICAgdmFyIHNwYWNlc0luTWF0Y2ggPSByZWdleGVzW3JneElkeF0uc291cmNlLnNwbGl0KCdcXFxccycpLmxlbmd0aDtcbiAgICAgICAgaWYgKHNwYWNlc0luTWF0Y2ggPiAxKSB7XG4gICAgICAgICAgdmFyIG11bHRpcGxlUGFydCA9IFtdO1xuICAgICAgICAgIGZvciAodmFyIHBhcnRKb2luID0gaWk7IHBhcnRKb2luID4gaWkgLSBzcGFjZXNJbk1hdGNoICYmIHBhcnRKb2luID49IDA7IHBhcnRKb2luLS0pIHtcbiAgICAgICAgICAgIG11bHRpcGxlUGFydC5wdXNoKHRoaXMucGFydHNbcGFydEpvaW5dKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgbXVsdGlwbGVQYXJ0LnJldmVyc2UoKTtcbiAgICAgICAgICBtdWx0aXBsZVBhcnQgPSBtdWx0aXBsZVBhcnQuam9pbignICcpO1xuICAgICAgICAgIG1hdGNoTXVsdGlwbGVQYXJ0ID0gcmVnZXhlc1tyZ3hJZHhdLmV4ZWMobXVsdGlwbGVQYXJ0KTtcblxuICAgICAgICAgIGlmIChtYXRjaE11bHRpcGxlUGFydCkge1xuICAgICAgICAgICAgLy8gaWYgd2UgaGF2ZSBhIDJuZCBjYXB0dXJlIGdyb3VwLCB0aGVuIHJlcGxhY2UgdGhlIGl0ZW0gd2l0aFxuICAgICAgICAgICAgLy8gdGhlIHRleHQgb2YgdGhhdCBncm91cFxuICAgICAgICAgICAgaWYgKG1hdGNoTXVsdGlwbGVQYXJ0WzJdKSB7XG4gICAgICAgICAgICAgIHRoaXMucGFydHMuc3BsaWNlKGlpIC0gc3BhY2VzSW5NYXRjaCArIDEsIHNwYWNlc0luTWF0Y2gsIG1hdGNoTXVsdGlwbGVQYXJ0WzJdKTtcbiAgICAgICAgICAgICAgaWkgLT0gc3BhY2VzSW5NYXRjaCArIDE7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBvdGhlcndpc2UsIGp1c3QgcmVtb3ZlIHRoZSBlbGVtZW50XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdGhpcy5wYXJ0cy5zcGxpY2UoaWkgLSBzcGFjZXNJbk1hdGNoICsgMSwgc3BhY2VzSW5NYXRjaCk7XG4gICAgICAgICAgICAgIGlpIC09IHNwYWNlc0luTWF0Y2ggKyAxO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAvLyBzZXQgdGhlIGZpZWxkXG4gICAgICAgICAgICB0aGlzW2ZpZWxkTmFtZV0gPSBsb29rdXBzW3JneElkeF0gfHwgbWF0Y2hNdWx0aXBsZVBhcnRbMV07XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRoaXM7XG59O1xuXG4vKipcbiAgIyMjIyBBZGRyZXNzI2V4dHJhY3RTdHJlZXRcblxuICBUaGlzIGZ1bmN0aW9uIGlzIHVzZWQgdG8gcGFyc2UgdGhlIGFkZHJlc3MgcGFydHMgYW5kIGxvY2F0ZSBhbnkgcGFydHNcbiAgdGhhdCBsb29rIHRvIGJlIHJlbGF0ZWQgdG8gYSBzdHJlZXQgYWRkcmVzcy5cbioqL1xucHJvdG8uZXh0cmFjdFN0cmVldCA9IGZ1bmN0aW9uKHJlZ2V4ZXMsIHJlU3BsaXRTdHJlZXQsIHJlTm9TdHJlZXQpIHtcbiAgdmFyIHJlTnVtZXJpY2VzcXVlID0gL14oXFxkKnxcXGQqXFx3KSQvO1xuICB2YXIgcGFydHMgPSB0aGlzLnBhcnRzO1xuICB2YXIgc3RyZWV0UGFydHNMZW5ndGggPSAyO1xuXG4gIC8vIGVuc3VyZSB3ZSBoYXZlIHJlZ2V4ZXNcbiAgcmVnZXhlcyA9IHJlZ2V4ZXMgfHwgW107XG5cbiAgLy8gVGhpcyBmdW5jdGlvbiBpcyB1c2VkIHRvIGxvY2F0ZSB0aGUgXCJiZXN0XCIgc3RyZWV0IHBhcnQgaW4gYW4gYWRkcmVzc1xuICAvLyBzdHJpbmcuICBJdCBpcyBjYWxsZWQgb25jZSBhIHN0cmVldCByZWdleCBoYXMgbWF0Y2hlZCBhZ2FpbnN0IGEgcGFydFxuICAvLyBzdGFydGluZyBmcm9tIHRoZSBsYXN0IHBhcnQgYW5kIHdvcmtpbmcgdG93YXJkcyB0aGUgZnJvbnQuIEluIHRlcm1zIG9mXG4gIC8vIHdoYXQgaXMgY29uc2lkZXJlZCB0aGUgYmVzdCwgd2UgYXJlIGxvb2tpbmcgZm9yIHRoZSBwYXJ0IGNsb3Nlc3QgdG8gdGhlXG4gIC8vIHN0YXJ0IG9mIHRoZSBzdHJpbmcgdGhhdCBpcyBub3QgaW1tZWRpYXRlbHkgcHJlZml4ZWQgYnkgYSBudW1lcmljZXNxdWVcbiAgLy8gcGFydCAoZWcuIDEyMywgNDJBLCBldGMpLlxuICBmdW5jdGlvbiBsb2NhdGVCZXN0U3RyZWV0UGFydChzdGFydEluZGV4KSB7XG4gICAgdmFyIGJlc3RJbmRleCA9IHN0YXJ0SW5kZXg7XG5cbiAgICAvLyBpZiB0aGUgc3RhcnQgaW5kZXggaXMgbGVzcyB0aGFuIG9yIGVxdWFsIHRvIDAsIHRoZW4gcmV0dXJuXG4gICAgZm9yICh2YXIgaWkgPSBzdGFydEluZGV4LTE7IGlpID49IDA7IGlpLS0pIHtcbiAgICAgIC8vIGl0ZXJhdGUgb3ZlciB0aGUgc3RyZWV0IHJlZ2V4ZXMgYW5kIHRlc3QgdGhlbSBhZ2FpbnN0IHRoZSB2YXJpb3VzIHBhcnRzXG4gICAgICBmb3IgKHZhciByZ3hJZHggPSAwOyByZ3hJZHggPCByZWdleGVzLmxlbmd0aDsgcmd4SWR4KyspIHtcbiAgICAgICAgLy8gaWYgd2UgaGF2ZSBhIG1hdGNoLCB0aGVuIHByb2Nlc3NcbiAgICAgICAgaWYgKHJlZ2V4ZXNbcmd4SWR4XS50ZXN0KHBhcnRzW2lpXSkgJiYgcGFydHNbaWktMV0gJiYgKCEgcmVOdW1lcmljZXNxdWUudGVzdChwYXJ0c1tpaS0xXSkpKSB7XG4gICAgICAgICAgLy8gdXBkYXRlIHRoZSBiZXN0IGluZGV4IGFuZCBicmVhayBmcm9tIHRoZSBpbm5lciBsb29wXG4gICAgICAgICAgYmVzdEluZGV4ID0gaWk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYmVzdEluZGV4O1xuICB9IC8vIGxvY2F0ZUJlc3RTdHJlZXRQYXJ0XG5cbiAgLy8gaXRlcmF0ZSBvdmVyIHRoZSBzdHJlZXQgcmVnZXhlcyBhbmQgdGVzdCB0aGVtIGFnYWluc3QgdGhlIHZhcmlvdXMgcGFydHNcbiAgZm9yICh2YXIgcGFydElkeCA9IHBhcnRzLmxlbmd0aDsgcGFydElkeC0tOyApIHtcbiAgICBmb3IgKHZhciByZ3hJZHggPSAwOyByZ3hJZHggPCByZWdleGVzLmxlbmd0aDsgcmd4SWR4KyspIHtcbiAgICAgIC8vIGlmIHdlIGhhdmUgYSBtYXRjaCwgdGhlbiBwcm9jZXNzXG4gICAgICAvLyBpZiB0aGUgbWF0Y2ggaXMgb24gdGhlIGZpcnN0IHBhcnQgdGhvdWdoLCByZWplY3QgaXQgYXMgd2VcbiAgICAgIC8vIGFyZSBwcm9iYWJseSBkZWFsaW5nIHdpdGggYSB0b3duIG5hbWUgb3Igc29tZXRoaW5nIChlLmcuIFN0IEdlb3JnZSlcbiAgICAgIGlmIChyZWdleGVzW3JneElkeF0udGVzdChwYXJ0c1twYXJ0SWR4XSkgJiYgcGFydElkeCA+IDApIHtcbiAgICAgICAgdmFyIHN0YXJ0SW5kZXggPSBsb2NhdGVCZXN0U3RyZWV0UGFydChwYXJ0SWR4KTtcblxuICAgICAgICAvLyBpZiB3ZSBhcmUgZGVhbGluZyB3aXRoIGEgc3BsaXQgc3RyZWV0IChpLmUuIGZvbyByZCB3ZXN0KSBhbmQgdGhlXG4gICAgICAgIC8vIGFkZHJlc3MgcGFydHMgYXJlIGFwcHJvcHJpYXRlbHkgZGVsaW1pdGVkLCB0aGVuIGdyYWIgdGhlIG5leHQgcGFydFxuICAgICAgICAvLyBhbHNvXG4gICAgICAgIGlmIChyZVNwbGl0U3RyZWV0LnRlc3QocGFydHNbc3RhcnRJbmRleCArIDFdKSkge1xuICAgICAgICAgIHN0cmVldFBhcnRzTGVuZ3RoID0gMztcbiAgICAgICAgICBzdGFydEluZGV4ICs9IDE7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocmVOb1N0cmVldC50ZXN0KHBhcnRzW3N0YXJ0SW5kZXhdKSkge1xuICAgICAgICAgIHN0cmVldFBhcnRzTGVuZ3RoID0gMTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuX2V4dHJhY3RTdHJlZXRQYXJ0cyhzdGFydEluZGV4LCBzdHJlZXRQYXJ0c0xlbmd0aCk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gICMjIyMgQWRkcmVzcyNmaW5hbGl6ZVxuXG4gIFRoZSBmaW5hbGl6ZSBmdW5jdGlvbiB0YWtlcyBhbnkgcmVtYWluaW5nIHBhcnRzIHRoYXQgaGF2ZSBub3QgYmVlbiBleHRyYWN0ZWRcbiAgYXMgb3RoZXIgaW5mb3JtYXRpb24sIGFuZCBwdXNoZXMgdGhvc2UgZmllbGRzIGludG8gYSBnZW5lcmljIGByZWdpb25zYCBmaWVsZC5cbioqL1xucHJvdG8uZmluYWxpemUgPSBmdW5jdGlvbigpIHtcbiAgLy8gdXBkYXRlIHRoZSByZWdpb25zLCBkaXNjYXJkaW5nIGFueSBlbXB0eSBzdHJpbmdzLlxuICB0aGlzLnJlZ2lvbnMgPSB0aGlzLnBhcnRzLmpvaW4oJyAnKS5zcGxpdCgvXFwsXFxzPy8pLmZpbHRlcihmdW5jdGlvbiAocmVnaW9uKSB7XG4gICAgICByZXR1cm4gcmVnaW9uLmxlbmd0aDtcbiAgfSk7XG5cbiAgLy8gcmVzZXQgdGhlIHBhcnRzXG4gIHRoaXMucGFydHMgPSBbXTtcblxuICByZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICAjIyMjIEFkZHJlc3Mjc3BsaXRcblxuICBTcGxpdCB0aGUgYWRkcmVzcyBpbnRvIGl0J3MgY29tcG9uZW50IHBhcnRzLCBhbmQgcmVtb3ZlIGFueSBlbXB0eSBwYXJ0c1xuKiovXG5wcm90by5zcGxpdCA9IGZ1bmN0aW9uKHNlcGFyYXRvcikge1xuICAvLyBzcGxpdCB0aGUgc3RyaW5nXG4gIHZhciBuZXdQYXJ0cyA9IHRoaXMudGV4dC5zcGxpdChzZXBhcmF0b3IgfHwgJyAnKTtcblxuICB0aGlzLnBhcnRzID0gW107XG4gIGZvciAodmFyIGlpID0gMDsgaWkgPCBuZXdQYXJ0cy5sZW5ndGg7IGlpKyspIHtcbiAgICBpZiAobmV3UGFydHNbaWldKSB7XG4gICAgICB0aGlzLnBhcnRzW3RoaXMucGFydHMubGVuZ3RoXSA9IG5ld1BhcnRzW2lpXTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICAjIyMjIEFkZHJlc3MjdG9TdHJpbmdcblxuICBDb252ZXJ0IHRoZSBhZGRyZXNzIHRvIGEgc3RyaW5nIHJlcHJlc2VudGF0aW9uXG4qKi9cbnByb3RvLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gIHZhciBvdXRwdXQgPSAnJztcblxuICBpZiAodGhpcy5idWlsZGluZykge1xuICAgIG91dHB1dCArPSB0aGlzLmJ1aWxkaW5nICsgJ1xcbic7XG4gIH1cblxuICBpZiAodGhpcy5zdHJlZXQpIHtcbiAgICBvdXRwdXQgKz0gdGhpcy5udW1iZXIgPyB0aGlzLm51bWJlciArICcgJyA6ICcnO1xuICAgIG91dHB1dCArPSB0aGlzLnN0cmVldCArICdcXG4nO1xuICB9XG5cbiAgb3V0cHV0ICs9IHRoaXMucmVnaW9ucy5qb2luKCcsICcpICsgJ1xcbic7XG5cbiAgcmV0dXJuIG91dHB1dDtcbn07XG5cbnByb3RvLmxvY2F0ZV91bml0ID0gZnVuY3Rpb24oKXtcblx0bGV0IG1hdGNoPXRoaXMudGV4dC5tYXRjaCgvKFxcZCtcXHMqLVxccypcXGQrKS8pO1xuXHRpZighbWF0Y2gpIHJldHVybjtcblx0bWF0Y2g9bWF0Y2hbMF07XG5cdHRoaXMudGV4dD10aGlzLnRleHQucmVwbGFjZShtYXRjaCwnJyk7XG5cdG1hdGNoPW1hdGNoLnJlcGxhY2UoL1xccy9nLCcnKS5zcGxpdCgnLScpO1xuXHR0aGlzLm51bWJlcj1tYXRjaFswXTtcblx0dGhpcy51bml0PW1hdGNoWzFdO1xufVxuXG5wcm90by5sb2NhdGVfbnVtYmVyID0gZnVuY3Rpb24oKXtcblx0aWYoIXRoaXMubnVtYmVyICYmICF0aGlzLnN0cmVldCl7IC8vbW9zdCBwcm9iYWJseSBwYXJ0IHdpdGggbnVtYmVycyBvbmx5IGlzIHRoZSBob21lIG51bWJlclxuXHRcdGxldCBudW1iZXI9dGhpcy5wYXJ0cy5maW5kKHBhcnQ9PnBhcnQubWF0Y2goL15cXGQrJC8pKTtcblx0XHRpZihudW1iZXIpe1xuXHRcdFx0dGhpcy5udW1iZXI9bnVtYmVyO1xuXHRcdFx0dGhpcy5wYXJ0cy5zcGxpY2UodGhpcy5wYXJ0cy5pbmRleE9mKG51bWJlciksMSk7XG5cdFx0fVxuXHR9XG59XG4iLCIvKiBqc2hpbnQgbm9kZTogdHJ1ZSAqL1xuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiAgIyBhZGRyZXNzaXRcblxuICBBZGRyZXNzSXQgaXMgYSBmcmVlZm9ybSBzdHJlZXQgYWRkcmVzcyBwYXJzZXIsIHRoYXQgaXMgZGVzaWduZWQgdG8gdGFrZSBhXG4gIHBpZWNlIG9mIHRleHQgYW5kIGNvbnZlcnQgdGhhdCBpbnRvIGEgc3RydWN0dXJlZCBhZGRyZXNzIHRoYXQgY2FuIGJlXG4gIHByb2Nlc3NlZCBpbiBkaWZmZXJlbnQgc3lzdGVtcy5cblxuICBUaGUgZm9jYWwgcG9pbnQgb2YgYGFkZHJlc3NpdGAgaXMgb24gdGhlIHN0cmVldCBwYXJzaW5nIGNvbXBvbmVudCwgcmF0aGVyXG4gIHRoYW4gYXR0ZW1wdGluZyB0byBhcHByb3ByaWF0ZWx5IGlkZW50aWZ5IHZhcmlvdXMgc3RhdGVzLCBjb3VudGllcywgdG93bnMsXG4gIGV0YywgYXMgdGhlc2UgdmFyeSBmcm9tIGNvdW50cnkgdG8gY291bnRyeSBmYWlybHkgZHJhbWF0aWNhbGx5LiBUaGVzZVxuICBkZXRhaWxzIGFyZSBpbnN0ZWFkIHB1dCBpbnRvIGEgZ2VuZXJpYyByZWdpb25zIGFycmF5IHRoYXQgY2FuIGJlIGZ1cnRoZXJcbiAgcGFyc2VkIGJhc2VkIG9uIHlvdXIgYXBwbGljYXRpb24gbmVlZHMuXG5cbiAgIyMgRXhhbXBsZSBVc2FnZVxuXG4gIFRoZSBmb2xsb3dpbmcgaXMgYSBzaW1wbGUgZXhhbXBsZSBvZiBob3cgYWRkcmVzcyBpdCBjYW4gYmUgdXNlZDpcblxuICBgYGBqc1xuICB2YXIgYWRkcmVzc2l0ID0gcmVxdWlyZSgnYWRkcmVzc2l0Jyk7XG5cbiAgLy8gcGFyc2UgYSBtYWRlIHVwIGFkZHJlc3MsIHdpdGggc29tZSBzbGlnaHRseSB0cmlja3kgcGFydHNcbiAgdmFyIGFkZHJlc3MgPSBhZGRyZXNzaXQoJ1Nob3AgOCwgNDMxIFN0IEtpbGRhIFJkIE1lbGJvdXJuZScpO1xuICBgYGBcblxuICBUaGUgYGFkZHJlc3NgIG9iamVjdCB3b3VsZCBub3cgY29udGFpbiB0aGUgZm9sbG93aW5nIGluZm9ybWF0aW9uOlxuXG4gIGBgYFxuICB7IHRleHQ6ICc4LzQzMSBTVCBLSUxEQSBSRCBNRUxCT1VSTkUnLFxuICAgIHBhcnRzOiBbXSxcbiAgICB1bml0OiA4LFxuICAgIGNvdW50cnk6IHVuZGVmaW5lZCxcbiAgICBudW1iZXI6IDQzMSxcbiAgICBzdHJlZXQ6ICdTVCBLSUxEQSBSRCcsXG4gICAgcmVnaW9uczogWyAnTUVMQk9VUk5FJyBdIH1cbiAgYGBgXG5cbiAgRm9yIG1vcmUgZXhhbXBsZXMsIHNlZSB0aGUgdGVzdHMuXG5cbiAgIyMgUmVmZXJlbmNlXG5cbioqL1xuXG4vKipcbiAgIyMjIGFkZHJlc3NpdChpbnB1dCwgb3B0cz8pXG5cbiAgUnVuIHRoZSBhZGRyZXNzIHBhcnNlciBmb3IgdGhlIGdpdmVuIGlucHV0LiAgT3B0aW9uYWwgYG9wdHNgIGNhbiBiZVxuICBzdXBwbGllZCBpZiB5b3Ugd2FudCB0byBvdmVycmlkZSB0aGUgZGVmYXVsdCAoRU4pIHBhcnNlci5cblxuKiovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGlucHV0LCBvcHRzKSB7XG4gIC8vIGlmIG5vIGxvY2FsZSBoYXMgYmVlbiBzcGVjaWZpZWQsIHRoZW4gdXNlIHRoZSBkZWZhdWx0IHZhbmlsbGEgZW4gbG9jYWxlXG4gIHZhciBwYXJzZSA9IChvcHRzIHx8IHt9KS5sb2NhbGUgfHwgcmVxdWlyZSgnLi9sb2NhbGUvZW4tVVMnKTtcblxuICAvLyBwYXJzZSB0aGUgYWRkcmVzc1xuICByZXR1cm4gcGFyc2UoaW5wdXQsIG9wdHMpO1xufTtcbiIsInZhciBwYXJzZXIgPSByZXF1aXJlKCcuLi9wYXJzZXJzL2VuLmpzJyk7XG52YXIgZXh0ZW5kID0gcmVxdWlyZSgnY29nL2V4dGVuZCcpO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKGlucHV0LCBvcHRzKSB7XG4gIC8vIHBhcnNlIHRoZSBiYXNlIGFkZHJlc3NcbiAgcmV0dXJuIHBhcnNlcihpbnB1dCwgZXh0ZW5kKHsgXG4gIFx0c3RhdGU6IHtcblx0ICAgIEFMOiAvKF5hbGFiYW1hfF5BTCQpL2ksXG5cdCAgICBBSzogLyheYWxhc2thfF5BSyQpL2ksXG5cdCAgICBBUzogLyheYW1lcmljYW5cXHNzYW1vYXxeQVMkKS9pLFxuXHQgICAgQVo6IC8oXmFyaXpvbmF8XkFaJCkvaSxcblx0ICAgIEFSOiAvKF5hcmthbnNhc3xeQVIkKS9pLFxuXHQgICAgQ0E6IC8oXmNhbGlmb3JuaWF8XkNBJCkvaSxcblx0ICAgIENPOiAvKF5jb2xvcmFkb3xeQ08kKS9pLFxuXHQgICAgQ1Q6IC8oXmNvbm5lY3RpY3V0fF5DVCQpL2ksXG5cdCAgICBERTogLyheZGVsYXdhcmV8XkRFJCkvaSxcblx0ICAgIERDOiAvKF5kaXN0cmljdFxcc29mXFxzY29sdW1iaWF8XkRDJCkvaSxcblx0ICAgIEZNOiAvKF5mZWRlcmF0ZWRcXHNzdGF0ZXNcXHNvZlxcc21pY3JvbmVzaWF8XkZNJCkvaSxcblx0ICAgIEZMOiAvKF5mbG9yaWRhfF5GTCQpL2ksXG5cdCAgICBHQTogLyheZ2VvcmdpYXxeR0EkKS9pLFxuXHQgICAgR1U6IC8oXmd1YW18XkdVJCkvaSxcblx0ICAgIEhJOiAvKF5oYXdhaWl8XkhJJCkvaSxcblx0ICAgIElEOiAvKF5pZGFob3xeSUQkKS9pLFxuXHQgICAgSUw6IC8oXmlsbGlub2lzfF5JTCQpL2ksXG5cdCAgICBJTjogLyheaW5kaWFuYXxeSU4kKS9pLFxuXHQgICAgSUE6IC8oXmlvd2F8XklBJCkvaSxcblx0ICAgIEtTOiAvKF5rYW5zYXN8XktTJCkvaSxcblx0ICAgIEtZOiAvKF5rZW50dWNreXxeS1kkKS9pLFxuXHQgICAgTEE6IC8oXmxvdWlzaWFuYXxeTEEkKS9pLFxuXHQgICAgTUU6IC8oXm1haW5lfF5NRSQpL2ksXG5cdCAgICBNSDogLyhebWFyc2hhbGxcXHNpc2xhbmRzfF5NSCQpL2ksXG5cdCAgICBNRDogLyhebWFyeWxhbmR8Xk1EJCkvaSxcblx0ICAgIE1BOiAvKF5tYXNzYWNodXNldHRzfF5NQSQpL2ksXG5cdCAgICBNSTogLyhebWljaGlnYW58Xk1JJCkvaSxcblx0ICAgIE1OOiAvKF5taW5uZXNvdGF8Xk1OJCkvaSxcblx0ICAgIE1TOiAvKF5taXNzaXNzaXBwaXxeTVMkKS9pLFxuXHQgICAgTU86IC8oXm1pc3NvdXJpfF5NTyQpL2ksXG5cdCAgICBNVDogLyhebW9udGFuYXxeTVQkKS9pLFxuXHQgICAgTkU6IC8oXm5lYnJhc2thfF5ORSQpL2ksXG5cdCAgICBOVjogLyhebmV2YWRhfF5OViQpL2ksXG5cdCAgICBOSDogLyhebmV3XFxzaGFtcHNoaXJlfF5OSCQpL2ksXG5cdCAgICBOSjogLyhebmV3XFxzamVyc2V5fF5OSiQpL2ksXG5cdCAgICBOTTogLyhebmV3XFxzbWV4aWNvfF5OTSQpL2ksXG5cdCAgICBOWTogLyhebmV3XFxzeW9ya3xeTlkkKS9pLFxuXHQgICAgTkM6IC8oXm5vcnRoXFxzY2Fyb2xpbmF8Xk5DJCkvaSxcblx0ICAgIE5EOiAvKF5ub3J0aFxcc2Rha290YXxeTkQkKS9pLFxuXHQgICAgTVA6IC8oXm5vcnRoZXJuXFxzbWFyaWFuYVxcc2lzbGFuZHN8Xk1QJCkvaSxcblx0ICAgIE9IOiAvKF5vaGlvfF5PSCQpL2ksXG5cdCAgICBPSzogLyheb2tsYWhvbWF8Xk9LJCkvaSxcblx0ICAgIE9SOiAvKF5vcmVnb258Xk9SJCkvaSxcblx0ICAgIFBXOiAvKF5wYWxhdXxeUFckKS9pLFxuXHQgICAgUEE6IC8oXnBlbm5zeWx2YW5pYXxeUEEkKS9pLFxuXHQgICAgUFI6IC8oXnB1ZXJ0b1xcc3JpY298XlBSJCkvaSxcblx0ICAgIFJJOiAvKF5yaG9kZVxcc2lzbGFuZHxeUkkkKS9pLFxuXHQgICAgU0M6IC8oXnNvdXRoXFxzY2Fyb2xpbmF8XlNDJCkvaSxcblx0ICAgIFNEOiAvKF5zb3V0aFxcc2Rha290YXxeU0QkKS9pLFxuXHQgICAgVE46IC8oXnRlbm5lc3NlZXxeVE4kKS9pLFxuXHQgICAgVFg6IC8oXnRleGFzfF5UWCQpL2ksXG5cdCAgICBVVDogLyhedXRhaHxeVVQkKS9pLFxuXHQgICAgVlQ6IC8oXnZlcm1vbnR8XlZUJCkvaSxcblx0ICAgIFZJOiAvKF52aXJnaW5cXHNpc2xhbmRzfF5WSSQpL2ksXG5cdCAgICBWQTogLyhedmlyZ2luaWF8XlZBJCkvaSxcblx0ICAgIFdBOiAvKF53YXNoaW5ndG9ufF5XQSQpL2ksXG5cdCAgICBXVjogLyhed2VzdFxcc3ZpcmdpbmlhfF5XViQpL2ksXG5cdCAgICBXSTogLyhed2lzY29uc2lufF5XSSQpL2ksXG5cdCAgICBXWTogLyhed3lvbWluZ3xeV1kkKS9pXG4gIFx0fSxcbiAgXHRjb3VudHJ5OiB7XG4gICAgICAgIFVTQTogLyheVU5JVEVEXFxzU1RBVEVTfF5VXFwuP1NcXC4/QT8kKS9pXG4gICAgfSxcbiAgICByZVBvc3RhbENvZGU6IC8oXlxcZHs1fSQpfCheXFxkezV9LVxcZHs0fSQpLyB9LCBvcHRzKSk7XG4gICAgICAgICAgICAgICAvLyBQb3N0YWwgY29kZXMgb2YgdGhlIGZvcm0gJ0RERERELUREREQnIG9yIGp1c3QgJ0REREREJ1xuICAgICAgICAgICAgICAgLy8gMTAwMTAgaXMgdmFsaWQgYW5kIHNvIGlzIDEwMDEwLTEyMzRcbn07XG4iLCIvKiBqc2hpbnQgbm9kZTogdHJ1ZSAqL1xuJ3VzZSBzdHJpY3QnO1xuXG4vKipcbiMjIGNvZy9leHRlbmRcblxuYGBganNcbnZhciBleHRlbmQgPSByZXF1aXJlKCdjb2cvZXh0ZW5kJyk7XG5gYGBcblxuIyMjIGV4dGVuZCh0YXJnZXQsICopXG5cblNoYWxsb3cgY29weSBvYmplY3QgcHJvcGVydGllcyBmcm9tIHRoZSBzdXBwbGllZCBzb3VyY2Ugb2JqZWN0cyAoKikgaW50b1xudGhlIHRhcmdldCBvYmplY3QsIHJldHVybmluZyB0aGUgdGFyZ2V0IG9iamVjdCBvbmNlIGNvbXBsZXRlZDpcblxuYGBganNcbmV4dGVuZCh7IGE6IDEsIGI6IDIgfSwgeyBjOiAzIH0sIHsgZDogNCB9LCB7IGI6IDUgfSkpO1xuYGBgXG5cblNlZSBhbiBleGFtcGxlIG9uIFtyZXF1aXJlYmluXShodHRwOi8vcmVxdWlyZWJpbi5jb20vP2dpc3Q9NjA3OTQ3NSkuXG4qKi9cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odGFyZ2V0KSB7XG4gIFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKS5mb3JFYWNoKGZ1bmN0aW9uKHNvdXJjZSkge1xuICAgIGlmICghIHNvdXJjZSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGZvciAodmFyIHByb3AgaW4gc291cmNlKSB7XG4gICAgICB0YXJnZXRbcHJvcF0gPSBzb3VyY2VbcHJvcF07XG4gICAgfVxuICB9KTtcblxuICByZXR1cm4gdGFyZ2V0O1xufTsiLCIvKiBqc2hpbnQgbm9kZTogdHJ1ZSAqL1xuJ3VzZSBzdHJpY3QnO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKHRleHRSZWdleGVzKSB7XG4gIHZhciByZWdleGVzID0gW107XG4gIHZhciByZVN0cmVldENsZWFuZXIgPSAvXlxcXj8oLiopXFwsP1xcJD8kLztcbiAgdmFyIGlpO1xuXG4gIGZvciAoaWkgPSB0ZXh0UmVnZXhlcy5sZW5ndGg7IGlpLS07ICkge1xuICAgIHJlZ2V4ZXNbaWldID0gbmV3IFJlZ0V4cChcbiAgICAgIHRleHRSZWdleGVzW2lpXS5yZXBsYWNlKHJlU3RyZWV0Q2xlYW5lciwgJ14kMVxcLD8kJyksXG4gICAgICAnaSdcbiAgICApO1xuICB9IC8vIGZvclxuXG4gIHJldHVybiByZWdleGVzO1xufTsiLCIvKiBqc2hpbnQgbm9kZTogdHJ1ZSAqL1xuJ3VzZSBzdHJpY3QnO1xuXG52YXIgQWRkcmVzcyA9IHJlcXVpcmUoJy4uL2FkZHJlc3MnKTtcbnZhciBjb21waWxlciA9IHJlcXVpcmUoJy4vY29tcGlsZXInKTtcblxuLy8gaW5pdGlhbGlzZSB0aGUgc3RyZWV0IHJlZ2V4ZXNcbi8vIHRoZXNlIGFyZSB0aGUgcmVnZXhlcyBmb3IgZGV0ZXJtaW5pbmcgd2hldGhlciBvciBub3QgYSBzdHJpbmcgaXMgYSBzdHJlZXRcbi8vIGl0IGlzIGltcG9ydGFudCB0byBub3RlIHRoYXQgdGhleSBhcmUgcGFyc2VkIHRocm91Z2ggdGhlIHJlU3RyZWV0Q2xlYW5lclxuLy8gcmVnZXggdG8gYmVjb21lIG1vcmUgc3RyaWN0XG4vLyB0aGlzIGxpc3QgaGFzIGJlZW4gc291cmNlZCBmcm9tOlxuLy8gaHR0cHM6Ly93d3cucHJvcGVydHlhc3Npc3Quc2EuZ292LmF1L3BhL3FoZWxwLnBodG1sP2NtZD1zdHJlZXR0eXBlXG4vL1xuLy8gX19OT1RFOl9fIFNvbWUgb2YgdGhlIHN0cmVldCB0eXBlcyBoYXZlIGJlZW4gZGlzYWJsZWQgZHVlIHRvIGNvbGxpc2lvbnNcbi8vIHdpdGggY29tbW9uIHBhcnRzIG9mIHN1YnVyYiBuYW1lcy4gIEF0IHNvbWUgcG9pbnQgdGhlIHN0cmVldCBwYXJzZXIgbWF5IGJlXG4vLyBpbXByb3ZlZCB0byBkZWFsIHdpdGggdGhlc2UgY2FzZXMsIGJ1dCBmb3Igbm93IHRoaXMgaGFzIGJlZW4gZGVlbWVkXG4vLyBzdWl0YWJsZS5cblxudmFyIHN0cmVldFJlZ2V4ZXMgPSBjb21waWxlcihbXG4gICdBTExFP1knLCAgICAgICAgICAgICAgIC8vIEFMTEVZIC8gQUxMWVxuICAnQVBQKFJPQUNIKT8nLCAgICAgICAgICAvLyBBUFBST0FDSCAvIEFQUFxuICAnQVJDKEFERSk/JywgICAgICAgICAgICAvLyBBUkNBREUgLyBBUkNcbiAgJ0FWKEV8RU5VRSk/JywgICAgICAgICAgLy8gQVZFTlVFIC8gQVYgLyBBVkVcbiAgJyhCT1VMRVZBUkR8QkxWRCknLCAgICAgLy8gQk9VTEVWQVJEIC8gQkxWRFxuICAnQlJPVycsICAgICAgICAgICAgICAgICAvLyBCUk9XXG4gICdCWVBBKFNTKT8nLCAgICAgICAgICAgIC8vIEJZUEFTUyAvIEJZUEFcbiAgJ0MoQVVTRSk/V0FZJywgICAgICAgICAgLy8gQ0FVU0VXQVkgLyBDV0FZXG4gICcoQ0lSQ1VJVHxDQ1QpJywgICAgICAgIC8vIENJUkNVSVQgLyBDQ1RcbiAgJ0NJUkMoVVMpPycsICAgICAgICAgICAgLy8gQ0lSQ1VTIC8gQ0lSQ1xuICAnQ0woT1NFKT8nLCAgICAgICAgICAgICAvLyBDTE9TRSAvIENMXG4gICdDTz9QU0UnLCAgICAgICAgICAgICAgIC8vIENPUFNFIC8gQ1BTRVxuICAnKENPUk5FUnxDTlIpJywgICAgICAgICAvLyBDT1JORVIgLyBDTlJcbiAgLy8gJ0NPVkUnLCAgICAgICAgICAgICAgICAgLy8gQ09WRVxuICAnKEMoKE9VUil8Uik/VHxDUlQpJywgICAvLyBDT1VSVCAvIENUIC9DUlRcbiAgJyhDUnxDUkVTfENSRVNDRU5UKScsICAgICAgICAgIC8vIENSRVNDRU5UIC8gQ1JFU1xuICAnRFIoSVZFKT8nLCAgICAgICAgICAgICAvLyBEUklWRSAvIERSXG4gIC8vICdFTkQnLCAgICAgICAgICAgICAgICAgIC8vIEVORFxuICAnRVNQKExBTkFERSk/JywgICAgICAgIC8vIEVTUExBTkFERSAvIEVTUFxuICAvLyAnRkxBVCcsICAgICAgICAgICAgICAgICAvLyBGTEFUXG4gICdGKFJFRSk/V0FZJywgICAgICAgICAgIC8vIEZSRUVXQVkgLyBGV0FZXG4gICcoRlJPTlRBR0V8RlJOVCknLCAgICAgIC8vIEZST05UQUdFIC8gRlJOVFxuICAvLyAnKEdBUkRFTlN8R0ROUyknLCAgICAgICAvLyBHQVJERU5TIC8gR0ROU1xuICAnKEdMQURFfEdMRCknLCAgICAgICAgICAvLyBHTEFERSAvIEdMRFxuICAvLyAnR0xFTicsICAgICAgICAgICAgICAgICAvLyBHTEVOXG4gICdHUihFRSk/TicsICAgICAgICAgICAgIC8vIEdSRUVOIC8gR1JOXG4gIC8vICdHUihPVkUpPycsICAgICAgICAgICAgIC8vIEdST1ZFIC8gR1JcbiAgLy8gJ0goRUlHSCk/VFMnLCAgICAgICAgICAgLy8gSEVJR0hUUyAvIEhUU1xuICAnKEhJR0hXQVl8SFdZKScsICAgICAgICAvLyBISUdIV0FZIC8gSFdZXG4gICcoTEFORXxMTiknLCAgICAgICAgICAgIC8vIExBTkUgLyBMTlxuICAnTElOSycsICAgICAgICAgICAgICAgICAvLyBMSU5LXG4gICdMT09QJywgICAgICAgICAgICAgICAgIC8vIExPT1BcbiAgJ01BTEwnLCAgICAgICAgICAgICAgICAgLy8gTUFMTFxuICAnTUVXUycsICAgICAgICAgICAgICAgICAvLyBNRVdTXG4gICcoUEFDS0VUfFBDS1QpJywgICAgICAgIC8vIFBBQ0tFVCAvIFBDS1RcbiAgJ1AoQVJBKT9ERScsICAgICAgICAgICAgLy8gUEFSQURFIC8gUERFXG4gIC8vICdQQVJLJywgICAgICAgICAgICAgICAgIC8vIFBBUktcbiAgJyhQQVJLV0FZfFBLV1kpJywgICAgICAgLy8gUEFSS1dBWSAvIFBLV1lcbiAgJ1BMKEFDRSk/JywgICAgICAgICAgICAgLy8gUExBQ0UgLyBQTFxuICAnUFJPTShFTkFERSk/JywgICAgICAgICAvLyBQUk9NRU5BREUgLyBQUk9NXG4gICdSRVMoRVJWRSk/JywgICAgICAgICAgIC8vIFJFU0VSVkUgLyBSRVNcbiAgLy8gJ1JJP0RHRScsICAgICAgICAgICAgICAgLy8gUklER0UgLyBSREdFXG4gICdSSVNFJywgICAgICAgICAgICAgICAgIC8vIFJJU0VcbiAgJ1IoT0EpP0QnLCAgICAgICAgICAgICAgLy8gUk9BRCAvIFJEXG4gICdST1cnLCAgICAgICAgICAgICAgICAgIC8vIFJPV1xuICAnU1EoVUFSRSk/JywgICAgICAgICAgICAvLyBTUVVBUkUgLyBTUVxuICAnU1RSRUVUfFNUUnxTVCcsICAgICAgICAvLyBTVFJFRVQgLyBTVFIgLyBTVFxuICAnU1RSST9QJywgICAgICAgICAgICAgICAvLyBTVFJJUCAvIFNUUlBcbiAgJ1RBUk4nLCAgICAgICAgICAgICAgICAgLy8gVEFSTlxuICAnVChFUlJBKT9DRXxURVI/UicsICAgICAvLyBURVJSQUNFIC8gVEVSIC8gVEVSUiAvIFRDRVxuICAnKFRIT1JPVUdIRkFSRXxURlJFKScsICAvLyBUSE9ST1VHSEZBUkUgLyBURlJFXG4gICdUUkFDSz8nLCAgICAgICAgICAgICAgIC8vIFRSQUNLIC8gVFJBQ1xuICAnVFIoQUkpP0wnLCAgICAgICAgICAgICAvLyBUUkFJTCAvIFRSTFxuICAnVChSVU5LKT9XQVknLCAgICAgICAgICAvLyBUUlVOS1dBWSAvIFRXQVlcbiAgLy8gJ1ZJRVcnLCAgICAgICAgICAgICAgICAgLy8gVklFV1xuICAnVkk/U1RBJywgICAgICAgICAgICAgICAvLyBWSVNUQSAvIFZTVEFcbiAgJ1dBTEsnLCAgICAgICAgICAgICAgICAgLy8gV0FMS1xuICAnV0E/WScsICAgICAgICAgICAgICAgICAvLyBXQVkgLyBXWVxuICAnVyhBTEspP1dBWScsICAgICAgICAgICAvLyBXQUxLV0FZIC8gV1dBWVxuICAnWUFSRCcsICAgICAgICAgICAgICAgICAvLyBZQVJEXG4gICdCUk9BRFdBWSdcbl0pO1xuXG52YXIgcmVTcGxpdFN0cmVldCA9IC9eKE58TlRIfE5PUlRIfEV8RVNUfEVBU1R8U3xTVEh8U09VVEh8V3xXU1R8V0VTVClcXCwkL2k7XG52YXIgcmVOb1N0cmVldCA9IGNvbXBpbGVyKFsnQlJPQURXQVknXSkucG9wKCk7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24odGV4dCwgb3B0cykge1xuICB2YXIgYWRkcmVzcyA9IG5ldyBBZGRyZXNzKHRleHQsIG9wdHMpO1xuXG4gIC8vIGNsZWFuIHRoZSBhZGRyZXNzXG4gIGFkZHJlc3NcbiAgICAuY2xlYW4oW1xuICAgICAgICAvLyByZW1vdmUgdHJhaWxpbmcgZG90cyBmcm9tIHR3byBsZXR0ZXIgYWJicmV2aWF0aW9uc1xuICAgICAgICBmdW5jdGlvbihpbnB1dCkge1xuICAgICAgICAgICAgcmV0dXJuIGlucHV0LnJlcGxhY2UoLyhcXHd7Mn0pXFwuL2csICckMScpO1xuICAgICAgICB9LFxuXG4gICAgICAgIC8vIGNvbnZlcnQgc2hvcCB0byBhIHVuaXQgZm9ybWF0XG4gICAgICAgIGZ1bmN0aW9uKGlucHV0KSB7XG4gICAgICAgICAgICByZXR1cm4gaW5wdXQucmVwbGFjZSgvXlxccypTSE9QXFxzPyhcXGQqKVxcLD9cXHMqL2ksICckMS8nKTtcbiAgICAgICAgfVxuICAgIF0pO1xuXHRcdFxuXHRhZGRyZXNzLmxvY2F0ZV91bml0KCk7XG5cdFxuXHRhZGRyZXNzXG5cbiAgICAvLyBzcGxpdCB0aGUgYWRkcmVzc1xuICAgIC5zcGxpdCgvXFxzLylcblxuICAgIC8vIGV4dHJhY3QgdGhlIHVuaXRcbiAgICAuZXh0cmFjdCgndW5pdCcsIFtcblxuICAgICAgICAoL14oPzpcXCN8QVBUfEFQQVJUTUVOVClcXHM/KFxcZCspLyksXG4gICAgICAgICgvXihcXGQrKVxcLyguKikvKVxuICAgIF0pXG5cbiAgICAvLyBleHRyYWN0IHRoZSBzdHJlZXRcbiAgICAuZXh0cmFjdFN0cmVldChzdHJlZXRSZWdleGVzLCByZVNwbGl0U3RyZWV0LCByZU5vU3RyZWV0KTtcblxuICBhZGRyZXNzLmxvY2F0ZV9udW1iZXIoKTtcblxuICBpZiAob3B0cyAmJiBvcHRzLnN0YXRlKSB7XG4gICAgYWRkcmVzcy5leHRyYWN0KCdzdGF0ZScsIG9wdHMuc3RhdGUgKTtcbiAgfVxuXG4gIGlmIChvcHRzICYmIG9wdHMuY291bnRyeSkge1xuICAgIGFkZHJlc3MuZXh0cmFjdCgnY291bnRyeScsIG9wdHMuY291bnRyeSApO1xuICB9XG5cbiAgaWYgKG9wdHMgJiYgb3B0cy5yZVBvc3RhbENvZGUpIHtcbiAgICBhZGRyZXNzLmV4dHJhY3QoJ3Bvc3RhbGNvZGUnLCBbIG9wdHMucmVQb3N0YWxDb2RlIF0pO1xuICB9XG5cbiAgIC8vIHRha2UgcmVtYWluaW5nIHVua25vd24gcGFydHMgYW5kIHB1c2ggdGhlbVxuICAgcmV0dXJuIGFkZHJlc3MuZmluYWxpemUoKTtcbn07XG4iXX0=
